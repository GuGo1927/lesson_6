import Vue from "vue";
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    dark: false,
    themes: {
      light: {
        primary: "#3f51b5",
        secondary: "#b0bec5",
        accent: "#8c9eff",
        titleText: "#000000",
        desc: "#999999",
        error: "#b71c1c"
      },
      dark: {
        primary: "#3f51b5",
        secondary: "#b0bec5",
        accent: "#8c9eff",
        titleText: "#ffffff",
        desc: "#ffffff",
        error: "#b71c1c"
      }
    }
  }
});
